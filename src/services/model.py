import torch
from loguru import logger
from transformers import pipeline

torch.set_num_threads(1)


class EmotionClassifier:
    """
    Emmotion classifier based on pre-trained model.

    Methods:
        predict_emotion: Predict emotion by given text.
    """

    model = pipeline(model="seara/rubert-tiny2-ru-go-emotions")

    @classmethod
    def predict_emotion(cls, text: str):
        """
        Predict emotion by given text.

        Args:
            text (str): Text for prediction.

        Returns:
            dict: Result dict.
        """
        result = cls.model(text)
        logger.info(result)
        return result


if __name__ == "__main__":
    clf = EmotionClassifier()
    result = clf.predict_emotion("You are the best!")
    print(result)
