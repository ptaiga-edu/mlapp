import os

from celery import Celery

backend_host = os.environ.get("backend_host", "localhost")
broker_host = os.environ.get("broker_host", "localhost")

celery_app = Celery(
    backend=f"redis://{backend_host}:6379",
    broker=f"amqp://guest:guest@{broker_host}:5672",
)
