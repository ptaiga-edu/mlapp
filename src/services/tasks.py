import time

from src.services.model import EmotionClassifier
from src.services.worker import celery_app


@celery_app.task(name="check")
def task_check_api(text):
    """Check FastAPI, Celery, RabbitMQ, Redis pipeline."""
    time.sleep(text.count("."))
    return f"=== {' '.join(list(text))} ==="


@celery_app.task(name="predict")
def task_predict(text):
    """Predict emotion by given text."""
    try:
        result = EmotionClassifier.predict_emotion(text)
        return result
    except Exception as ex:
        logger.error([ex])
        return None


if __name__ == "__main__":
    celery_app.start()
