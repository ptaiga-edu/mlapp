#!/usr/bin/env python
import sys

import pika


def send(queue_name: str, message: str):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters("localhost")
    )
    channel = connection.channel()

    channel.queue_declare(queue=queue_name)
    channel.basic_publish(exchange="", routing_key=queue_name, body=message)

    print(f" [x] Sent '{message}' to '{queue_name}'")

    connection.close()


if __name__ == "__main__":
    if len(sys.argv) > 1:
        queue_name = sys.argv[1]
        message = sys.argv[2]
    else:
        queue_name = "hello"
        message = "Hello World!"
    send(queue_name, message)
