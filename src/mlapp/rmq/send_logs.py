#!/usr/bin/env python
import sys

import pika


def send(exchange_name: str, message: str):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters("localhost")
    )
    channel = connection.channel()

    channel.exchange_declare(exchange=exchange_name, exchange_type="fanout")

    channel.basic_publish(exchange=exchange_name, routing_key="", body=message)
    print(f" [x] Sent '{message}' to exchange '{exchange_name}'")
    connection.close()


if __name__ == "__main__":
    exchange_name = sys.argv[1] if len(sys.argv) > 1 else "logs"
    message = " ".join(sys.argv[2:]) or "info: Hello World!"
    send(exchange_name, message)
