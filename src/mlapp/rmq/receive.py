#!/usr/bin/env python
import os
import sys
import time

import pika


def callback(ch, method, properties, body):
    print(f" [x] Received {body.decode()}")
    time.sleep(body.count(b"."))
    print(" [x] Done")


def main():
    if len(sys.argv) > 1:
        queue_name = sys.argv[1]
    else:
        queue_name = "hello"

    connection = pika.BlockingConnection(
        pika.ConnectionParameters("localhost")
    )
    channel = connection.channel()

    channel.queue_declare(queue=queue_name)

    channel.basic_consume(
        queue=queue_name, auto_ack=True, on_message_callback=callback
    )

    print(" [*] Waiting for messages. To exit press CTRL+C")
    channel.start_consuming()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
