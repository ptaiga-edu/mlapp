from fastapi import BackgroundTasks, FastAPI
from fastapi.middleware.cors import CORSMiddleware
from loguru import logger

from src.api.router import router as api_router
from src.api.utils import print_logger_info
from src.services.worker import celery_app


def get_app() -> FastAPI:
    """
    FastAPI app initialization.
    """
    fastapi_app = FastAPI(
        title="Emotion classification service",
        version="0.1.0",
        debug=False,
        description="ML service for classifying emotions in text",
    )
    fastapi_app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["GET", "POST"],
        allow_headers=["*"],
    )

    fastapi_app.include_router(api_router, prefix="/api")
    logger.info("FastAPI application has been initialized")
    return fastapi_app


app = get_app()


@app.get("/", name="hello")
async def root():
    return {"message": "Hello!"}


@app.get("/check", name="check_api")
async def check_api(text: str, background_tasks: BackgroundTasks):
    background_tasks.add_task(
        print_logger_info,
        text,
        "Check API",
    )
    async_result = celery_app.send_task("check", args=[text])
    result = async_result.get()
    return {"received": text, "result": result}


@app.get("/predict", name="predict")
async def predict(text: str, background_tasks: BackgroundTasks):
    async_result = celery_app.send_task("predict", args=[text])
    result = async_result.get()
    background_tasks.add_task(print_logger_info, text, result)
    return {"received": text, "emotion": result}
