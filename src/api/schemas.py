"""Schemas for endpoints."""

from pydantic import BaseModel


class HealthcheckResult(BaseModel):
    is_alive: bool
    date: str


class TextRequest(BaseModel):
    """Schema of request for prediction.

    Attributes:
        text (str): Text for analysis.
    """

    text: str

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "text": "Hi! How are you doing? Nice to meet you!",
                }
            ]
        }
    }
