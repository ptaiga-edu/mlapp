from fastapi import APIRouter, BackgroundTasks

from src.api.schemas import TextRequest
from src.api.utils import print_logger_info
from src.services.worker import celery_app

router = APIRouter()


@router.post("/predict/")
async def predict_emotion(
    text_request: TextRequest, background_tasks: BackgroundTasks
):
    """Emotion prediction.

    Args:
        text_request (TextRequest): Text request.

    Returns:
        dict: Result dict.
    """

    async_result = celery_app.send_task("predict", args=[text_request.text])

    result = async_result.get()
    background_tasks.add_task(
        print_logger_info,
        text_request.text,
        result,
    )

    return result
