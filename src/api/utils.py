from datetime import datetime

from loguru import logger


async def print_logger_info(input_text: str, result):
    """
    print_logger_info - printing logger.
    """
    logger.info({"input_text": input_text, "result": result})


def return_current_time():
    """
    Example func, return current time.
    """
    return datetime.utcnow().isoformat()
