from datetime import datetime

import requests  # type: ignore

import gradio as gr

BACKEND_HOST = "http://127.0.0.1:8000"
PREDICT_ENDPOINT = "/api/predict/"


def predict(text: str, save_history: bool, history: list[str]):
    url = BACKEND_HOST + PREDICT_ENDPOINT
    payload = {"text": text}
    response = requests.post(url, json=payload).json()

    now = datetime.utcnow().isoformat(sep=" ", timespec="seconds")
    result = "Time: " + now
    result += "\nText: " + text
    result += "\nEmotion: " + str(response)

    if save_history:
        history.append(result)
        result = "\n\n".join(history[::-1])

    print(text, save_history, history, sep="\n")
    return result, history


demo = gr.Interface(
    fn=predict,
    inputs=["text", "checkbox", gr.State(value=[])],
    outputs=["text", gr.State()],
)

demo.launch()
