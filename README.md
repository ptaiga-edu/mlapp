# MLApp

MLApp service is a web application that provides an API for interacting with a machine learning model. It allows users to send queries with prediction data and get results back.

When launched, the application initializes FastAPI, which handles HTTP requests. The app also connects to the machine learning model and loads it into memory for use in making predictions.


## Usage

Run:
```{bash}
$ docker compose up
```

Stop:
```{bash}
$ docker compose down
```

API documentaion: http://127.0.0.1:8000/docs

Experimental fronted (run after `docker compose up` and open in browser http://127.0.0.1:7860):
```{bash}
$ python -m src.gradio.frontend
```


### Old API

Predict request via browser link:
http://127.0.0.1:8000/predict?text=how+are+you+doing

Or via Curl:
```{bash}
curl -G http://127.0.0.1:8000/predict \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d 'text=How+are+you+doing'
```

Response would be similar: `{"received":"how are you doing","emotion":[{"label":"neutral","score":0.7290613651275635}]}`.

**Healthcheck**:

 - http://127.0.0.1:8000/ &mdash; check only FastAPI response.

 - http://127.0.0.1:8000/check?text=...example &mdash; check connection between FastAPI and Celery (with RabbitMQ as brocker and Redis as backend). Response should be after 3 seconds (the number of dots in parameter '?text=' corresponds to the number of seconds for response).


### New API

POST request:
```{bash}
curl -X 'POST' \
  'http://127.0.0.1:8000/api/predict/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "text": "Hello! How are you doing?"
}'
```

**Healthcheck**:

 - http://127.0.0.1:8000/api/healthcheck &mdash; check only FastAPI response.


## Develop


### Environment

Create environment and install all dependencies:

```{bash}
$ conda env create -n mlapp -f conda.yaml
$ conda activate mlapp
$ pdm install
```

To install only production packages add apropriate flag to latest line: `pdm install --prod`


### RabbitMQ + Redis

Run RabbitMQ with management on http://127.0.0.1:15672 (guest/guest):
```{bash}
$ docker run -d --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.13-management
```

Run Redis:
```{bash}
$ docker run -d --rm --name redis -p 6379:6379 redis
```

Also both services could be running by:
```{bash}
$ docker compose up rmq redis
```


### Celery

_Celery_-worker could be running:
```{bash}
$ celery -A src.services.tasks worker --loglevel=INFO
```

For autoreaload in development mode (`watchdog` must be installed in virtual environment):
```{bash}
$ watchmedo auto-restart --directory=./src --pattern=*.py --recursive -- celery -A src.services.tasks worker --loglevel=INFO
```

Celery uses RabbitMQ as broker and Redis as backend in configuration:
```{python}
celery_app = Celery(
    backend="redis://localhost:6379",
    broker="amqp://guest@localhost:5672"
)
```

But also possible used only RabbitMQ as backend and broker:
```{python}
celery_app = Celery(
    backend="rpc://", broker="amqp://guest@localhost:5672"
)
```


### FastAPI

#### Standard mode:
```{bash}
$ uvicorn src.app:app
# OR (but don't resolve packages properly)
# fastapi run src/app.py
```

#### Development mode:
```{bash}
$ uvicorn src.app:app --reload --reload-include ./src/
# OR (but don't resolve packages properly)
# fastapi dev src/app.py
```

#### Docker mode:
```{bash}
$ docker build . -t app
$ docker run -it --rm -p 8000:8000 app
```
For experiments with RabbitMQ use --network="host", then 127.0.0.1 in your docker container will point to your docker host (not recommend for production).

#### Interact with RMQ-server:
```{bash}
$ docker exec rabbitmq rabbitmqctl {command}
```
{command} - status, list_queues, list_exchanges, list_bindings, ...

Send message to queue and receive it:
```{bash}
$ python src/mlapp/rmq/send.py {queue_name} {message}
$ python src/mlapp/rmq/receive.py {queue_name}
```

Send logs via exchanger:
```{bash}
$ python src/mlapp/rmq/send_logs.py {exchange_name} {logs}
$ python src/mlapp/rmq/receive_logs.py {exchange_name}
```


## Linting

```{bash}
$ black .
$ isort .
$ flake8 .
$ mypy .
```
