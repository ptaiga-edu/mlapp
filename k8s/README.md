# Kubernetes


## Pod

Commands bellow have similar effect as: `$ docker run -it --rm -p 8080:80 -v ./://usr/share/nginx/html:ro nginx:alpine`

Start pod:
```
$ kubectl create -f nginx-pod.yaml
$ kubectl expose pod nginx-pod --port=8080 --target-port=80
```

Proxy server:
```
$ minikube service nginx-pod
```

Remove pod:
```
$ kubectl delete services nginx-pod
$ kubectl delete pods nginx-pod
```


## Deployment

Create Development object and expose it via NodePort type of Service:
```
$ kubectl create -f nginx-deployment.yaml
$ kubectl expose deployment/nginx-deployment --type="NodePort" --port 8080 --target-port 80
```

Check connection:
```
$ export NODE_PORT=$(kubectl get services/nginx-deployment -o go-template='{{(index .spec.ports 0).nodePort}}')
$ echo NODE_PORT=$NODE_PORT
$ curl $(minikube ip):$NODE_PORT
```

Remove created objects:
```
$ kubectl delete service nginx-deployment
$ kubectl delete deployment nginx-deployment
```


## Ingress

Create Development object and expose it via Service:
```
$ kubectl create -f nginx-deployment.yaml
$ kubectl expose deployment/nginx-deployment --port 8080 --target-port 80
```

Enable Ingress addon:
```
$ minikube addons enable ingress
```

Create Ingress object:
```
$ kubectl create -f nginx-ingress.yaml
```

Check forwarding traffic from Ingress to Development via Service:
```
$ curl $(minikube ip)
```

If add line `<$(minikube ip)>   webserver.lan` to `/etc/hosts` when try:
```
$ curl webserver.lan
```

Remove all created objects:
```
$ kubectl delete ingress nginx-ingress
$ kubectl delete service nginx-deployment
$ kubectl delete deployment nginx-deployment
```
