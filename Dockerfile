FROM mambaorg/micromamba

WORKDIR /app

COPY conda.yaml ./

RUN micromamba create -n venv -f conda.yaml

ENV PDM_CHECK_UPDATE=false

COPY pyproject.toml pdm.lock README.md ./

RUN micromamba run -n venv pdm install --prod --check

COPY ./src/ ./src/

ENTRYPOINT ["micromamba", "run", "-n", "venv", "pdm", "run"]

CMD ["uvicorn", "src.app:app", "--host", "0.0.0.0", "--port", "8000"]
